package com.hero.org;
public class PersonDirector {
	private PersonBuilder builder;
	public PersonDirector(PersonBuilder builder) {
		// TODO Auto-generated constructor stub
		this.builder = builder;
	}
	public void construct() {
		builder.setPerson("hero", "宝马汽车", 25);
	}
	public Person getPerson() {
		return builder.getPerson();
	}

}
