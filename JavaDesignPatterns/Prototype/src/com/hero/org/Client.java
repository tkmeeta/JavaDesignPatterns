package com.hero.org;

import java.util.ArrayList;

public class Client {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ConcretePrototype prototype = new ConcretePrototype();
		prototype.setAddress("123");
		prototype.setPhone("123456789");
		prototype.setName("herozhou1314");
		ArrayList<Model> models = new ArrayList<Model>();
		for (int i = 0; i < 2; i++) {
			Model model = new Model();
			model.setAddress("123");
			model.setPhone("123456789");
			model.setName("herozhou1314");
			models.add(model);
		}
		prototype.setModels(models);
		
		ConcretePrototype prototype2 = (ConcretePrototype) prototype.clone();
		prototype2.setName("herozhou123456");
		prototype2.getModels().get(0).setName("abcd");
		System.out.println("prototype2-->" + prototype2);
		System.out.println("prototype2-->" + prototype);
	}
}
