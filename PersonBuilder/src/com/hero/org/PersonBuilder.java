package com.hero.org;

public abstract class PersonBuilder {
	public abstract void setPerson(String name, String address, int age);
	public abstract Person getPerson();
}
