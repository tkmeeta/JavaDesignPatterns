package com.hero.org;

public class Decorator implements Component {
	Component component;

	public Decorator(Component component) {
		// TODO Auto-generated constructor stub
		this.component = component;
	}

	@Override
	public void watchTv() {
		// TODO Auto-generated method stub
		this.component.watchTv();
	}
}
