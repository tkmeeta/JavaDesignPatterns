/**
 * The RefinedAbstraction
 */
public class TextItalic extends Text {
	private TextImp imp;

	public TextItalic(Class<?> class1) {
		imp = GetTextImp(class1);
	}
	public void DrawText(String text) {
		System.out.println(text);
		System.out.println("The text is italic text!");
		imp.DrawTextImp();
	}
}