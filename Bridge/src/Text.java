/**
 * The Abstract of Text
 */
public abstract class Text {
	protected TextImp GetTextImp(Class<?> type) {
		TextImp text = null;
		try {
			text = (TextImp) Class.forName(type.getName()).newInstance();
		} catch (Exception e) {
			System.out.println("创建出错");
		}
		return text;
	}
	public abstract void DrawText(String text);
}