package com.StarMicronics.StarIOSDK;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.LinearLayout;


public class helpMessage extends Activity 
{
	private static String m_message;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.helpmessage);
		
		WebView web = new WebView(this);
		LinearLayout layout = (LinearLayout)findViewById(R.id.linearLayout_main);
		layout.addView(web);
		web.loadData(PrinterTypeActivity.HTMLCSS() + m_message, "text/html", "utf-16");
	}
	
	public static void SetMessage(String message)
	{
		m_message = message;
	}
}
