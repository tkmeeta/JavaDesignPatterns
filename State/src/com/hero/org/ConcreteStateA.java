package com.hero.org;

public class ConcreteStateA extends State {
	@Override
	public void Handle(Context context) {
		System.out.println(" 这是状态 StateA");
		context.setState(new ConcreteStateB());
	}
}
