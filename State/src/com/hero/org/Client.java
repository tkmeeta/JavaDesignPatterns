package com.hero.org;

public class Client {

	public static void main(String[] args) {
		Context c = new Context(new ConcreteStateA());
		System.out.println("c--->>" + (c.getState() instanceof ConcreteStateA));
		c.request();
		c.request();
		c.request();
		c.request();
		c.request();
		c.request();
		c.request();
		c.request();
	}
}
